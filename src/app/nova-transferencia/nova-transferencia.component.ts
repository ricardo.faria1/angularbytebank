import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Transferencia } from '../models/transferencia.model';
import { TransferenciaService } from '../services/transferencia.service';


@Component({
    selector: 'bb-nova-transferencia',
    templateUrl: './nova-transferencia.component.html',
    styleUrls: ['./nova-transferencia.component.scss']
})
export class NovaTransferenciaComponent implements OnInit {

    @Output() aoTansferir = new EventEmitter<any>();

    valor!: number;
    destino!: number;

    constructor(private transferenciaService: TransferenciaService, private router: Router) {

    }

    ngOnInit() {
    }

    transferir() {
      const valorEmitir: Transferencia = {valor: this.valor, destino: this.destino};

      this.transferenciaService.adicionar(valorEmitir).subscribe(resultado => {
        console.log(resultado);
        this.limparCampos();
        this.router.navigateByUrl('extrato')
      }, error => console.error(error));
    }
    limparCampos() {
      this.valor = 0
      this.destino = 0;
    }
}
